#include "enemy.h"
#include <QDebug>

Enemy::Enemy()
{
    //ustawienie losowej pozycji
    int arrayPos[] = {52, 374 ,698};
    int random_number = rand() % 3;
    setPos(arrayPos[random_number],52);

    //ustawienie enemy
    this->setPixmap(QPixmap(":/textures/enemy_down.png"));

    QTimer * timer = new QTimer();
    //połączenie z slotem move
    //connect(jaki sygnał chce połączyć, SIGNAL(), z czym chce połączyć, SLOT(funkcja którą chce wywoływać))
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    //co 50 milisekund timer będzie wywoływać funkcję move
    timer->start(40);
}

void Enemy::checkCollision()
{
    //sprawdzenie czy enemy koliduje z wall
    //lista wszystkich obiektów QGraphics
    QList<QGraphicsItem *> items = collidingItems();

    for(int i=0; i<items.size(); i++)
    {
        //sprawdzenie typu obiektu QGraphics
        if(typeid(*(items[i])) == typeid(Wall)){
            switch (kierunek) {
            case 0:
                setPos(pos().x(), pos().y()-2);
                break;
            case 1:
                setPos(pos().x(), pos().y()+2);
                break;
            case 2:
                setPos(pos().x()+2, pos().y());
                break;
            case 3:
                setPos(pos().x()-2, pos().y());
                break;
            }
            changeDirection();
        }

        if(typeid(*(items[i])) == typeid(Player)){
            Shoot();
        }

        /*
        if(typeid(*(items[i])) == typeid(Enemy) && items[i] != this){
            qDebug()<<"kolizja";
            x *= -1;
            y *= -1;
        }
        */

    }
}

void Enemy::changeDirection()
{
    random_number = rand() % 4;
}

void Enemy::move()
{

    checkCollision();
    //poruszanie przeciwnika
    switch (random_number) {
    case 0:
        kierunek = 0;
        x = 0;
        y = 2;
        //polewidzenia -> setRect(0,50,50,200);
        Shoot();
        this->setPixmap(QPixmap(":/textures/enemy_down.png"));
        break;
    case 1:
        kierunek = 1;
        x = 0;
        y = -2;
        //polewidzenia -> setRect(0,-200,50,200);
        this->setPixmap(QPixmap(":/textures/enemy_up.png"));
        break;
    case 2:
        kierunek = 2;
        x = -2;
        y = 0;
        //polewidzenia -> setRect(-200,0,200,50);
        this->setPixmap(QPixmap(":/textures/enemy_left.png"));
        break;
    case 3:
        kierunek = 3;
        x = 2;
        y = 0;
        //polewidzenia -> setRect(50,0,200,50);
        this->setPixmap(QPixmap(":/textures/enemy_right.png"));
        break;
    }
    setPos(pos().x()+x, pos().y()+y);
}

void Enemy::Shoot()
{
    //stworzenie posicku i dodanie do sceny
    Bullet * bullet = new Bullet(kierunek, "Enemy");
    bullet -> setPos(pos().x()+20, pos().y()+20);
    scene() -> addItem(bullet);
}

