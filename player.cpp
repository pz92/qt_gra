#include "player.h"
#include <QDebug>

//obsluga klawiszy do poruszania czolgiem
void Player::keyPressEvent(QKeyEvent *event)
{
    checkCollision();
    if(event->key() == Qt::Key_Left)
    {
        ostatniaPozycja[0] = this->pos().x();
        ostatniaPozycja[1] = this->pos().y();
        kierunek = 2;
        setPos(x()-10, y());
        this->setPixmap(QPixmap(":/textures/tank_left.png"));
    }

    else if(event->key() == Qt::Key_Right)
    {
        ostatniaPozycja[0] = this->pos().x();
        ostatniaPozycja[1] = this->pos().y();
        kierunek = 3;
        setPos(x()+10, y());
        this->setPixmap(QPixmap(":/textures/tank_right.png"));
    }

    else if(event->key() == Qt::Key_Up)
    {
        ostatniaPozycja[0] = this->pos().x();
        ostatniaPozycja[1] = this->pos().y();
        kierunek = 1;
        setPos(x(), y()-10);
        this->setPixmap(QPixmap(":/textures/tank_up.png"));
    }

    else if(event->key() == Qt::Key_Down)
    {
        ostatniaPozycja[0] = this->pos().x();
        ostatniaPozycja[1] = this->pos().y();
        kierunek = 0;
        setPos(x(), y()+10);
        this->setPixmap(QPixmap(":/textures/tank_down.png"));
    }

    if (event->key() == Qt::Key_Space)
    {
        Shoot();
    }
}

Player::Player()
{
    setPixmap(QPixmap(":/textures/tank_up.png"));
}

void Player::Shoot()
{
    //stworzenie posicku i dodanie do sceny
    Bullet * bullet = new Bullet(kierunek, "Player");
    bullet -> setPos(x()+20, y()+20);
    scene() -> addItem(bullet);
}

void Player::checkCollision()
{
    //sprawdzenie czy player koliduje z wall
    //lista wszystkich obiektów QGraphics
    QList<QGraphicsItem *> items = collidingItems();

    for(int i=0; i<items.size(); ++i)
    {
        //sprawdzenie typu obiektu QGraphics
        if(typeid(*(items[i])) == typeid(Wall)){
            setPos(ostatniaPozycja[0], ostatniaPozycja[1]);

            return;
        }

    }
}

//tworzenie przeciwnikow
void Player::spawn()
{
    Enemy * enemy = new Enemy();
    scene() -> addItem(enemy);

}
