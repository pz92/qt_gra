#include "game.h"


Game::Game()
{
    //stworzenie sceny
    QGraphicsScene * scene = new QGraphicsScene();


    //stworzenie obiektu i dodanie go do sceny
    Player * player = new Player();
    player -> setFlag(QGraphicsItem::ItemIsFocusable);
    player -> setFocus();

    //dodanie obiektu do sceny
    scene -> addItem(player);

    //stworzenie widoku
    QGraphicsView *view = new QGraphicsView();
    view -> setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view -> setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view -> setScene(scene);

    //wyswietlenie sceny
    view -> show();
    view -> setFixedSize(800,600);
    scene -> setSceneRect(0,0,800,600);
    player->setPos(375, 500);

    //tworzenie enemy

    QTimer * timer = new QTimer();
    QObject::connect(timer, SIGNAL(timeout()), player, SLOT(spawn()));
    timer -> start(5000);


    //tworzenie ścian
    Wall * wall;
    for(int i=0; i<40; i++)
    {
        wall = new Wall();
        wall -> setPos(50*i,0);
        scene -> addItem(wall);
    }

    for(int i=0; i<40; i++)
    {
        wall = new Wall();
        wall -> setPos(50*i,550);
        scene -> addItem(wall);
    }

    for(int i=1; i<39; i++)
    {
        wall = new Wall();
        wall -> setPos(0,50*i);
        scene -> addItem(wall);
    }

    for(int i=1; i<39; i++)
    {
        wall = new Wall();
        wall -> setPos(750,50*i);
        scene -> addItem(wall);
    }

}
