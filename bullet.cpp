#include "bullet.h"
#include <QDebug>
extern Game * game;

Bullet::Bullet(int kierunek, QString ktoStrzela)
{
    this->kierunek = kierunek;
    this->ktoStrzela = ktoStrzela;
    QTimer * timer = new QTimer();
    //połączenie z slotem move
    //connect(jaki sygnał chce połączyć, SIGNAL(), z czym chce połączyć, SLOT(funkcja którą chce wywoływać))
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    //co 50 milisekund timer będzie wywoływać funkcję move
    timer->start(10);
}



void Bullet::move()
{

    if(kierunek == 1)
    {
        setPixmap(QPixmap(":/textures/bullet_up.png"));
        //pocisk leci w gore
        setPos(x(), y()-2);
    }
    else if(kierunek == 0)
    {
        setPixmap(QPixmap(":/textures/bullet_down.png"));
        //pocisk leci w doł
        setPos(x(), y()+2);
    }
    else if(kierunek == 2)
    {
        setPixmap(QPixmap(":/textures/bullet_left.png"));
        //pocisk leci w lewo
        setPos(x()-2, y());
    }
    else
    {
        setPixmap(QPixmap(":/textures/bullet_right.png"));
        //pocisk leci w prawo
        setPos(x()+2, y());
    }
    remove();
    checkCollision();
}

//usuwanie pocisku poza ekranem
void Bullet::remove()
{
    if(pos().y() < 0 || pos().y() > 600 || pos().x() > 800 || pos().x() < 0)
    {
        //scene() -> removeItem(this);
        delete this;
    }
}

void Bullet::checkCollision()
{
    //sprawdzenie czy pocisk koliduje z enemy
    //lista wszystkich obiektów QGraphics
    QList<QGraphicsItem *> items = collidingItems();

    for(int i=0; i<items.size(); i++)
    {
        //sprawdzenie typu obiektu QGraphics
        if(ktoStrzela == "Enemy")
        {
            if(typeid(*(items[i])) == typeid(Player)){
                //scene() -> removeItem(items[i]);
                //scene() -> removeItem(this);
                delete items[i];
                delete this;
                return;
            }
        }
        else
        {
            if(typeid(*(items[i])) == typeid(Enemy)){
                delete items[i];
                delete this;
                return;

            }
        }
    }
}
