#ifndef ENEMY_H
#define ENEMY_H

#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QTimer>
#include <QObject>
#include <QGraphicsScene>
#include <stdlib.h> //random
#include <wall.h>

class Enemy: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Enemy();
    void checkCollision();
    void changeDirection();
    int random_number = 0;
    int kierunek;
    int x;
    int y;
    void Shoot();
public slots:
    void move();
};

#endif // ENEMY_H
