#ifndef PLAYER_H
#define PLAYER_H
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QKeyEvent>
#include "bullet.h"
#include <QObject>
#include <enemy.h>

class Player: public QObject, public QGraphicsPixmapItem{
        Q_OBJECT

public:
    int kierunek = 1;
    int ostatniaPozycja[2];
    void keyPressEvent(QKeyEvent * event);
    Player();
    void Shoot();
    void checkCollision();

public slots:
    void spawn();
};

#endif // PLAYER_H
