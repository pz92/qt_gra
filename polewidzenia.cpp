#include "polewidzenia.h"

PoleWidzenia::PoleWidzenia()
{
    this->setRect(0,0, 100, 200);

}
bool PoleWidzenia::checkCollision()
{
    //sprawdzenie czy enemy koliduje z wall
    //lista wszystkich obiektów QGraphics
    QList<QGraphicsItem *> items = collidingItems();

    for(int i=0; i<items.size(); i++)
    {
        //sprawdzenie typu obiektu QGraphics
        if(typeid(*(items[i])) == typeid(Player)){
            return true;
        }
    }
    return false;
}
