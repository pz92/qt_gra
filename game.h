#ifndef GAME_H
#define GAME_H
#include "mainwindow.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <player.h>
#include <QTimer>
#include <wall.h>

class Game
{
public:
    Game();
    void Show();
};

#endif // GAME_H
