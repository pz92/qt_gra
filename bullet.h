#ifndef BULLET_H
#define BULLET_H
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QObject>
#include <QGraphicsScene>
#include <QList>
#include <enemy.h>
#include <game.h>

class Bullet: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Bullet();
    Bullet(int kierunek, QString ktoStrzela);
    int kierunek;
    QString ktoStrzela;
public slots:
    void move();
    void remove();
    void checkCollision();
};

#endif // BULLET_H
